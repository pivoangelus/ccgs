//
//  Card.swift
//  CCGS
//
//  Created by Justin Andros on 2/26/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import Foundation
import SpriteKit

enum CardName: Int {
    
    case CreatureWolf = 0,
         CreatureBear,      // 1
         CreatureDragon,    // 2
         Energy,            // 3
         SpellDeathRay,     // 4
         SpellRabid,        // 5
         SpellSleep,        // 6
         SpellStoneSkin     // 7
    
}

class Card: SKSpriteNode {
    
    // MARK: Properities
    
    var faceUp = true
    let frontTexture: SKTexture
    let backTexture: SKTexture
    var largeTexture: SKTexture?
    let largeTextureFilename: String
    var enlarged = false
    var savedPostion = CGPointZero
    
    // MARK:
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    init(cardNamed: CardName) {
        // initialize properities
        backTexture = SKTexture(imageNamed: "card_back.png")
        
        switch cardNamed {
        case .CreatureWolf:
            frontTexture = SKTexture(imageNamed: "card_creature_wolf.png")
            largeTextureFilename = "card_creature_wolf_large.png"
        case .CreatureBear:
            frontTexture = SKTexture(imageNamed: "card_creature_bear.png")
            largeTextureFilename = "card_creature_bear_large.png"
        case .CreatureDragon:
            frontTexture = SKTexture(imageNamed: "card_creature_dragon.png")
            largeTextureFilename = "card_creature_dragon_large.png"
        case .Energy:
            frontTexture = SKTexture(imageNamed: "card_energy.png")
            largeTextureFilename = "card_engery_large.png"
        case .SpellDeathRay:
            frontTexture = SKTexture(imageNamed: "card_spell_death_ray.png")
            largeTextureFilename = "card_spell_death_ray_large.png"
        case .SpellRabid:
            frontTexture = SKTexture(imageNamed: "card_spell_rabid.png")
            largeTextureFilename = "card_spell_rabid_large.png"
        case .SpellSleep:
            frontTexture = SKTexture(imageNamed: "card_spell_sleep.png")
            largeTextureFilename = "card_spell_sleep_large.png"
        case .SpellStoneSkin:
            frontTexture = SKTexture(imageNamed: "card_spell_stoneskin.png")
            largeTextureFilename = "card_spell_stoneskin_large.png"
        }
        
        // call designated initializer on super
        super.init(texture: frontTexture, color: UIColor.clearColor(), size: frontTexture.size())
        
        // set properities defined in super
        userInteractionEnabled = true
    }
    
    func flip() {
        let firstHalfFlip = SKAction.scaleXTo(0.0, duration: 0.4)
        let secondHalfFlip = SKAction.scaleXTo(1.0, duration: 0.4)
        
        setScale(1.0)
        
        if faceUp {
            runAction(firstHalfFlip) {
                self.texture = self.backTexture
                if let damageLabel = self.childNodeWithName("damageLabel") {
                    damageLabel.hidden = true
                }
                self.faceUp = false
                self.runAction(secondHalfFlip)
            }
        } else {
            runAction(firstHalfFlip) {
                self.texture = self.frontTexture
                if let damageLabel = self.childNodeWithName("damageLabel") {
                    damageLabel.hidden = false
                }
                self.faceUp = true
                self.runAction(secondHalfFlip)
            }
        }
    }
    
    func enlarge() {
        // TODO: damage disappears when enlarged
        if enlarged {
            let slide = SKAction.moveTo(savedPostion, duration: 0.3)
            let scaleDown = SKAction.scaleTo(1.0, duration: 0.3)
            runAction(SKAction.group([slide, scaleDown])) {
                self.enlarged = false
                self.zPosition = 0
            }
        } else {
            enlarged = true
            savedPostion = position
            
            if largeTexture != nil {
                texture = largeTexture
            } else {
                largeTexture = SKTexture(imageNamed: largeTextureFilename)
                texture = largeTexture
            }
            
            zPosition = 20
            
            let newPosition = CGPointMake(CGRectGetMidX(parent!.frame), CGRectGetMidY(parent!.frame))
            removeAllActions()
            
            let slide = SKAction.moveTo(newPosition, duration: 0.3)
            let scaleUp = SKAction.scaleTo(5.0, duration: 0.3)
            runAction(SKAction.group([slide, scaleUp]))
        }
    }
    
    // MARK: Touches
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            // "doubletap" to flip the card
            if touch.tapCount > 1 {
                enlarge()
            }
            
            if enlarged { return }
            
            zPosition = 15
            let liftUp = SKAction.scaleTo(1.2, duration: 0.2)
            runAction(liftUp, withKey: "pickup")
            
            let wiggleIn = SKAction.scaleXTo(1.0, duration: 0.2)
            let wiggleOut = SKAction.scaleXTo(1.2, duration: 0.2)
            let wiggle = SKAction.sequence([wiggleIn, wiggleOut])
            let wiggleRepeat = SKAction.repeatActionForever(wiggle)
            runAction(wiggleRepeat, withKey: "wiggle")
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if enlarged { return }
        for touch in touches {
            let location = touch.locationInNode(scene!)
            let touchedNode = nodeAtPoint(location)
            touchedNode.position = location
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if enlarged { return }
        for _ in touches {
            zPosition = 0
            let dropDown = SKAction.scaleTo(1.0, duration: 0.2)
            runAction(dropDown, withKey: "drop")
            removeActionForKey("wiggle")
        }
    }
    
}
