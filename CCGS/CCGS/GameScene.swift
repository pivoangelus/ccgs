//
//  GameScene.swift
//  CCGS
//
//  Created by Justin Andros on 2/26/16.
//  Copyright (c) 2016 Justin Andros. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    // MARK:
    
    override func didMoveToView(view: SKView) {
        let wolf = Card(cardNamed: .CreatureWolf)
        wolf.position = CGPointMake(100, 200)
        addChild(wolf)
        
        let bear = Card(cardNamed: .CreatureBear)
        bear.position = CGPointMake(300, 200)
        addChild(bear)
        
        wolf.addChild(newDamageLabel())
        bear.addChild(newDamageLabel())
    }
    
    // MARK: Damage

    func newDamageLabel() -> SKLabelNode {
        // bad way to make a 'new' node, ignoring the warning -_-
        var damageLabel = SKLabelNode(fontNamed: "OpenSans-Bold")
        damageLabel.name = "damageLabel"
        damageLabel.fontSize = 12
        damageLabel.fontColor = UIColor(red: 0.47, green:  0.0, blue: 0.0, alpha: 1.0)
        damageLabel.text = "0"
        damageLabel.position = CGPointMake(25, 40)
        
        return damageLabel
    }
    
}
