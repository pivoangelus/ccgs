//
//  GameViewController.swift
//  CCGS
//
//  Created by Justin Andros on 2/26/16.
//  Copyright (c) 2016 Justin Andros. All rights reserved.
//

import UIKit
import SpriteKit

extension SKNode {
    
    class func unarchiveFromFile(file: String) -> SKNode? {
        do {
            let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks")
            let sceneData = try? NSData(contentsOfFile: path!, options: .DataReadingMappedIfSafe)
            let archiver = NSKeyedUnarchiver(forReadingWithData: sceneData!)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! GameScene
            archiver.finishDecoding()
            return scene
        }
    }
    
}

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene {
            // configure view
            let skView = self.view as! SKView
            skView.showsFPS = false
            skView.showsNodeCount = false
            
            // sprite kit applies additional optimizations to improve rendering performance
            skView.ignoresSiblingOrder = true
            
            // set the scale mode to sacle to fit the window
            scene.scaleMode = .AspectFill
            scene.backgroundColor = SKColor(white: 0.2, alpha: 1.0)
            
            skView.presentScene(scene)
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.AllButUpsideDown
        } else {
            return UIInterfaceOrientationMask.All
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning() 
    }

}
